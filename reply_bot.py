#!i/usr/bin/env python
import argparse
import asyncio
from contextlib import contextmanager, ExitStack
from datetime import datetime
import logging
import os
import re
import sys
import time
from traceback import format_exc

import aiofiles
import aiosqlite
from telethon import events, sessions, TelegramClient
from telethon.errors import FloodWaitError
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
import yaml

try:
    from contextlib import AsyncExitStack
except ImportError:
    from async_exit_stack import AsyncExitStack

HARD_DEBUG_LEVEL = 15
HARD_INFO_LEVEL = 25
logging.addLevelName(HARD_DEBUG_LEVEL, 'HARD_DEBUG')
logging.addLevelName(HARD_INFO_LEVEL, 'HARD_INFO')
logging.Formatter.converter = time.gmtime  # asctime in UTC
logger = logging.getLogger('reply_bot')
default_script_dir = os.path.dirname(__file__)
config_files_lock = asyncio.Lock()


def setup_logging(cutoff_level=HARD_INFO_LEVEL, dest=None):
    logging.root.setLevel(cutoff_level)
    formatter = logging.Formatter('%(asctime)sZ - %(levelname)s %(name)s: %(message)s')
    if dest == '-':
        class InfoFilter(logging.Filter):
            def filter(self, rec):
                return rec.levelno < logging.WARNING
        out_handler = logging.StreamHandler(sys.stdout)
        out_handler.setLevel(logging.NOTSET)
        out_handler.addFilter(InfoFilter())
        out_handler.setFormatter(formatter)
        logging.root.addHandler(out_handler)
        err_handler = logging.StreamHandler()
        err_handler.setLevel(logging.WARNING)
        err_handler.setFormatter(formatter)
        logging.root.addHandler(err_handler)
        return
    if dest is None:
        handler = logging.SysLogHandler(address='/dev/log')
    else:
        handler = logging.FileHandler(dest)
    handler.setLevel(cutoff_level)
    handler.setFormatter(formatter)
    logging.root.addHandler(handler)


async def read_configs(config_dir):
    async with config_files_lock:
        session_fn = os.path.join(config_dir, 'tg_sessions.yml')
        async with aiofiles.open(session_fn, 'rt') as fd:
            content = await fd.read()
        session_config = yaml.safe_load(content)
        clients = {}
        for session_username, config in session_config['sessions'].items():
            application_username = config['application_username']
            app = session_config['applications'][application_username]
            session_state = config['session_state'].replace('\n', '')
            session = sessions.StringSession(session_state)
            client = TelegramClient(session, app['api_id'], str(app['api_hash']))
            clients[session_username] = client
        django_db = os.path.join(config_dir, 'django.db.sqlite3')
        rules = {'auto_replies': {}}
        async with aiosqlite.connect(django_db) as db:
            auto_replies = rules['auto_replies']
            async with db.execute("SELECT session_id, pattern, message_text FROM core_reply WHERE reply_type = 'auto'") as cursor:
                async for row in cursor:
                    session_id, pattern, message_text = row
                    if session_id not in auto_replies:
                        auto_replies[session_id] = {}
                    if pattern not in auto_replies[session_id]:
                        auto_replies[session_id][pattern] = message_text
        return clients, rules


@contextmanager
def log_exceptions(suppress=False):
    try:
        yield
    except Exception:
        logger.error(format_exc())
        if not suppress:
            raise


class ConfigEventHandler(PatternMatchingEventHandler):
    def __init__(self, config_observer, **kwargs):
        super().__init__(**kwargs)
        self.config_observer = config_observer
        self.logger = logger

    def on_modified(self, event):
        self.logger.log(HARD_INFO_LEVEL, f'Changed {event.src_path}. Restarting...')
        self.config_observer.last_modified = datetime.utcnow()


class ConfigModifiedObserver():
    def __init__(self, path, patterns=['*.json', '*.yaml', '*.yml', '*.sqlite3'], loop=None, **kwargs):
        super().__init__()
        self.path = path
        self.loop = loop or asyncio.get_event_loop()
        self.handler_kwargs = {'patterns': patterns, **kwargs}
        self._last_modified = None
        self.observer = None

    async def __aenter__(self):
        self.last_modified = None
        self.observer = Observer()
        self.loop.run_in_executor(
            None,
            self.observer.schedule,
            ConfigEventHandler(self, **self.handler_kwargs),
            self.path
        )
        self.observer.start()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.observer.stop()
        self.observer.join()
        self.observer = None

    async def wait_until_modified(self, start_value=None):
        if start_value is None:
            start_value = self.last_modified
        while start_value == self.last_modified:
            await asyncio.sleep(.1)


@contextmanager
def always_cleanup_loop(loop=None):
    loop = loop or asyncio.get_event_loop()
    def multiple_raise(*errors):
        try:
            raise errors[-1]
        finally:
            if errors[:-1]:
                multiple_raise(*errors[:-1])
    try:
        yield loop
    except (KeyboardInterrupt, SystemExit, Exception) as e:
        cancelled = [task for task in asyncio.Task.all_tasks() if task.cancelled()]
        returned_errors = loop.run_until_complete(asyncio.gather(*cancelled, return_exceptions=True))
        non_cancelled = [e for e in returned_errors if not isinstance(e, asyncio.CancelledError)]
        if non_cancelled:
            multiple_raise(*non_cancelled)
        if not isinstance(e, (KeyboardInterrupt, SystemExit)):
            raise


@contextmanager
def TgClientListener(client, handler_bindings):
    for handler, event in handler_bindings:
        client.add_event_handler(handler, event)
    try:
        yield client
    finally:
        for handler, event in handler_bindings:
            client.remove_event_handler(handler, event)


class TgRules(ExitStack):
    def __init__(self, clients, rules, loop=None):
        self.clients = clients
        self.rules = rules
        self.loop = loop or asyncio.get_event_loop()
        self.bound_clients = {}
        super().__init__()

    def __enter__(self):
        listeners = []
        for session_username, session_rules in self.rules['auto_replies'].items():
            client = self.clients[session_username]
            self.bound_clients[session_username] = client
            handler_bindings = []
            for pattern, text in session_rules.items():
                pattern_kwargs = {}
                if pattern != '.*':
                    RE_PATTERN = re.compile(pattern, flags=re.I)
                    pattern_kwargs = {'pattern': lambda m, RE=RE_PATTERN: RE.search(m)}
                event = events.NewMessage(
                    **pattern_kwargs,
                    func=lambda event: event.is_private,
                    incoming=True
                )
                async def reply_handler(event, text=text):
                    chat = event.chat if event.chat else (await event.get_chat())
                    if chat.bot:
                        return
                    recipient = f'@{chat.username}' if chat.username else chat.id
                    try:
                        await event.reply(text, parse_mode='md', link_preview=False)
                    except FloodWaitError:
                        logger.log('Raised FloodWaitError for {recipient}')
                        raise
                    logger.log(HARD_DEBUG_LEVEL, f'Auto replied to {recipient}')
                event = events.NewMessage(
                    **pattern_kwargs,
                    func=lambda event: event.is_private,
                    incoming=True
                )
                handler_bindings.append((reply_handler, event))
            listeners.append(TgClientListener(client, handler_bindings))
        super().__enter__()
        for listener in listeners:
            self.enter_context(listener)
        return self

    def __exit__(self, *exc):
        suppress = super().__exit__(*exc)
        self.bound_clients.clear()
        return suppress


async def main(config_dir):
    async with ConfigModifiedObserver(config_dir) as config_observer:
        previous_last_modified = config_observer.last_modified
        previously_launched_users = set()
        while True:
            clients, rules = await read_configs(config_dir)
            with TgRules(clients, rules) as stack_rules:
                bound_clients = stack_rules.bound_clients
                async with AsyncExitStack() as stack_clients:
                    for session_username, client in bound_clients.items():
                        await stack_clients.enter_async_context(client)
                        should_catch_up = session_username not in previously_launched_users
                        if not should_catch_up:
                            continue
                        async def catch_up(client=client, username=session_username):
                            user_suffix = f' for {username}' if username else ''
                            try:
                                logger.debug(f'Catching up{user_suffix}...')
                                await client.catch_up()
                            finally:
                                logger.log(HARD_INFO_LEVEL, f'Catched up{user_suffix}.')
                        loop.create_task(catch_up())
                    try:
                        logger.log(HARD_INFO_LEVEL, f'Started listening ({len(bound_clients)} tg clients).')
                        await config_observer.wait_until_modified(previous_last_modified)  # main cycle
                    finally:
                        logger.info(f'Stopped listening ({len(bound_clients)} tg clients).')
                        previous_last_modified = config_observer.last_modified
                        previously_launched_users = set(bound_clients)
            await asyncio.sleep(.1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Auto replying bot.')
    parser.add_argument('--config-dir', metavar='DIR', default=default_script_dir,
                        help='Directory containing config files (default is script directory)')
    parser.add_argument('--loglevel', metavar='LEVEL', default='HARD_INFO', help='Minimum log level')
    parser.add_argument('--logdest', metavar='PATH',
                        help='Logging file (- for stdout/stderr, default to syslog)')
    args = parser.parse_args()
    try:
        log_level = int(args.loglevel)
    except ValueError:
        if f'{args.loglevel}_LEVEL' in vars():
            log_level = vars()[f'{args.loglevel}_LEVEL']
        else:
            log_level = logging.getLevelName(args.loglevel)
    setup_logging(log_level, dest=args.logdest)
    loop = asyncio.get_event_loop()
    with log_exceptions(suppress=False), always_cleanup_loop():
        loop.run_until_complete(main(args.config_dir))
