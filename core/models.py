from datetime import datetime, timezone
from enum import Enum
import re

from django.db import models


class ModifiedModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        self.modified_at = datetime.now(timezone.utc)
        if not self.created_at:
            self.created_at = self.modified_at
        return super().save(*args, **kwargs)


class TgApplication(ModifiedModel):
    username = models.CharField(max_length=64, primary_key=True)
    api_id = models.IntegerField(unique=True)
    api_hash = models.CharField(max_length=64)

    def __str__(self):
        return f'App {self.username} ({self.api_id})'


class TgSession(ModifiedModel):
    username = models.CharField(max_length=64, primary_key=True)
    has_credentials = models.BooleanField(default=False)
    application = models.ForeignKey(TgApplication, null=True, on_delete=models.SET_NULL)
    state = models.TextField(null=True)

    def __str__(self):
        return f'Session {self.username} (app from {self.application_id})'


class Reply(ModifiedModel):
    class ReplyType(Enum):
        auto = 'Auto Replies'

    reply_type = models.CharField(
      max_length=16,
      choices=[(tag.name, tag.value) for tag in ReplyType],
      default=ReplyType.auto.name
    )
    session = models.ForeignKey(TgSession, on_delete=models.PROTECT)
    pattern = models.CharField(max_length=128, null=True)
    message_text = models.TextField()

    # def __init__(self, *args, **kwargs):
    #     self._compiled_pattern = None
    #     self._compiled_regex = None
    #     return super().__init__(*args, **kwargs)

    # @property
    # def compiled_regex(self):
    #     if self._compiled_pattern == self.pattern and self._compiled_regex:
    #         return self._compiled_regex
    #     self._compiled_pattern = self.pattern
    #     if not self._compiled_pattern:
    #         self._compiled_pattern = r'^.*$'
    #     self._compiled_regex = re.compile(pattern, flags=re.I)
    #     return self._compiled_regex

    def __str__(self):
        return f'Pattern {self.pattern} (session from {self.session_id})'