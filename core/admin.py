from django.contrib import admin
from django.contrib.auth.models import User, Group
# from django.http import HttpResponseRedirect
# from django.urls import reverse

from .models import Reply


class ReplyAdmin(admin.ModelAdmin):
    list_display = ('pattern', 'reply_type', 'session_id')
admin.site.register(Reply, ReplyAdmin)
admin.site.site_header = 'Reply bot setup'
