import os

from django.conf import settings
from django.db import transaction
from watchdog.observers import Observer
from watchdog.events import FileModifiedEvent, PatternMatchingEventHandler
import yaml


def synchronize_configs(Model, get_model, models, yaml_dicts, protect_deleting=False):
    before = {m.username for m in models}
    after = set(yaml_dicts)
    added = after - before
    deleted = before - after

    def extract_fields(yaml_dict):
        fields = {}
        for k, v in yaml_dict.items():
            if k == 'application_username':
                k = 'application_id'
            elif k == 'session_state':  # pylint: disable=undefined-loop-variable
                k = 'state'
            fields[k] = v
        return fields
    for m in models:
        if m.username in deleted:
            if protect_deleting:
                assert Model == get_model('TgSession')
                m.has_credentials = False
                m.state = None
                m.save()
            else:
                m.delete()
        elif m.username in yaml_dicts:
            fields = extract_fields(yaml_dicts[m.username])
            for k, v in fields.items():
                setattr(m, k, v)
            m.save()
    for username in added:
        fields = extract_fields(yaml_dicts[username])
        m = Model(username=username, **fields)
        m.save()


class ConfigEventHandler(PatternMatchingEventHandler):
    def __init__(self, get_model, **kwargs):
        self.get_model = get_model
        super().__init__(**kwargs)

    def on_modified(self, event):
        with open(event.src_path, 'rt') as f:
            yaml_config = yaml.safe_load(f)
        with transaction.atomic():
            TgApplication = self.get_model('TgApplication')
            apps = TgApplication.objects.all()
            synchronize_configs(TgApplication, self.get_model, apps, yaml_config['applications'])
            TgSession = self.get_model('TgSession')
            sessions = TgSession.objects.all()
            synchronize_configs(TgSession, self.get_model, sessions, yaml_config['sessions'], protect_deleting=True)


def create_observer(get_model):
    tg_session_path = os.path.join(settings.BASE_DIR, 'tg_sessions.yml')
    event_handler = ConfigEventHandler(get_model, patterns=[tg_session_path])
    observer = Observer()
    watch = observer.schedule(event_handler, settings.BASE_DIR)
    observer.event_queue.put((FileModifiedEvent(tg_session_path), watch))
    return observer
