from django.apps import AppConfig

from .watchdog import create_observer


class CoreConfig(AppConfig):
    name = 'core'

    def ready(self):
        observer = create_observer(self.get_model)
        observer.start()