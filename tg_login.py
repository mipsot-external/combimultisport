#!/usr/bin/python3.6
import os

import yaml
from telethon import sessions, TelegramClient, sync

script_dir = os.path.dirname(__file__)
with open(os.path.join(script_dir, 'tg_sessions.yml'), 'rt') as fd:
    session_config = yaml.safe_load(fd)
session = sessions.StringSession()
api_id = int(input('api_id: '))
api_hashes = {app['api_id']: str(app['api_hash']) for app in session_config['applications'].values()}
api_hash = api_hashes.get(api_id)
if not api_hash:
    api_hash = input('api_hash: ')
with TelegramClient(session, api_id, api_hash):
    pass
print(session.save())
